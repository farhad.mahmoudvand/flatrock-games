﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreLib;
using CoreLib.DataModels;

namespace AdminPanel.Controllers
{
    public class ApiController : CoreLib.BaseController
    {
        public ApiController(quizgameContext context) : base(context)
        {

        }


        public JsonResult GetQuotes(string keyword, int page)
        {
            try
            {
                var result = new Models.QuotesResult();
                var data = db.Quotes.AsQueryable();
                var size = 10;
                page = page - 1;
                if (keyword.HasValue())
                {
                    data = data.Where(f => f.Text.Contains(keyword));
                }

                data = data.OrderByDescending(f => f.Id);

                var c = data.Count();
                result.PagesCount = c / size;
                if (c % size > 0) result.PagesCount++;
                result.CurrentPage = page;
                result.Quotes = data.Skip(page * size).Take(size).ToList()
                    .Select(f => new Models.Quote
                    {
                        Id = f.Id,
                        AuthorId = f.AuthorId,
                        AuthorName = db.Authors.Where(a => a.Id == f.AuthorId).Select(a => a.Name).FirstOrDefault(),
                        Text = f.Text,
                        LastModified = f.LastModified.ToString("dd MMM yyyy hh:mm:ss")
                    }).ToList();

                result.Authors = db.Authors.OrderBy(f => f.Name).ToList();
                return CreateJsonResult(result);
            }
            catch(Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult SaveQuote(int id, string text, int authorId, string authorName)
        {
            try
            {
                if (text.HasValue()) text = text.Trim();
                if (authorName.HasValue()) authorName = authorName.Trim();
                var item = new Quotes();
                var exists = db.Quotes.Any(f => f.Id == id);
                if (exists) item = db.Quotes.Find(id);

                if (authorName.HasValue())
                {
                    var authExists = db.Authors.FirstOrDefault(f => f.Name == authorName);
                    if (authExists != null) authorId = authExists.Id;
                    else
                    {
                        var auth = new Authors();
                        auth.Name = authorName;
                        db.Add(auth);
                        db.SaveChanges();
                        authorId = auth.Id;
                    }
                }

                item.AuthorId = authorId;
                item.Text = text;
                item.LastModified = DateTime.Now;
                
                if(!exists)
                {
                    item.CreateDate = DateTime.Now;
                    db.Quotes.Add(item);
                }
                db.SaveChanges();
                var result = new Models.Quote
                {
                    Id = item.Id,
                    AuthorId = item.AuthorId,
                    AuthorName = db.Authors.Where(a => a.Id == item.AuthorId).Select(a => a.Name).FirstOrDefault(),
                    Text = item.Text,
                    LastModified = item.LastModified.ToString("dd MMM yyyy hh:mm:ss")
                };
                return CreateJsonResult(result);
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult DeleteQuote(int id)
        {
            try
            {
                var quote = db.Quotes.Find(id);
                if (quote == null) return CreateErrorResult("Quote not found!");
                db.Quotes.Remove(quote);
                db.SaveChanges();
                return CreateJsonResult(true);
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult GetUsers(string keyword, int page)
        {
            try
            {
                var result = new CoreLib.Models.ListResult();
                var data = db.Users.AsQueryable();
                var size = 10;
                page = page - 1;
                if (keyword.HasValue())
                {
                    data = data.Where(f => f.Username.Contains(keyword) || f.FirstName.Contains(keyword) || f.LastName.Contains(keyword));
                }

                if (keyword.HasValue()) data = data.OrderByDescending(f => f.Username.StartsWith(keyword) || f.FirstName.StartsWith(keyword) || f.LastName.StartsWith(keyword));
                data = data.OrderByDescending(f => f.Id);

                var c = data.Count();
                result.PagesCount = c / size;
                if (c % size > 0) result.PagesCount++;
                result.CurrentPage = page;
                result.Data = data.Skip(page * size).Take(size).ToList();
                return CreateJsonResult(result);
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }
        
        public JsonResult SaveUser(int id, string username, string password, string firstName, string lastName)
        {
            try
            {
                if (username.IsNull()) return CreateErrorResult("Please fill the username!");
                if (password.IsNull() && id == 0) return CreateErrorResult("Please fill the password!");
                if (firstName.IsNull()) return CreateErrorResult("Please fill the first name!");
                if (lastName.IsNull()) return CreateErrorResult("Please fill the last name!");

                username = username.Trim().ToLower();
                firstName = firstName.Trim();
                lastName = lastName.Trim();
                
                var item = new Users();
                var exists = db.Users.Any(f => f.Id == id);
                if (exists) item = db.Users.Find(id);

                var usernameExists = db.Users.Any(f => f.Username == username);
                if (exists && usernameExists) usernameExists = username != item.Username;

                if (usernameExists) return CreateErrorResult("Username is already exists!");

                item.Username = username;
                item.FirstName = firstName;
                item.LastName = lastName;
                item.LastModified = DateTime.Now;

                if(id == 0) item.Password = password.HashMD5();
                else if(password.HasValue()) item.Password = password.HashMD5();

                if (!exists)
                {
                    item.CreateDate = DateTime.Now;
                    db.Users.Add(item);
                }
                db.SaveChanges();
                
                return CreateJsonResult(item);
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult DeleteUser(int id)
        {
            try
            {
                var item = db.Users.Find(id);
                if (item == null) return CreateErrorResult("User not found!");
                db.Users.Remove(item);
                db.SaveChanges();
                return CreateJsonResult(true);
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult GetLatestGames(int page)
        {
            try
            {
                var result = new CoreLib.Models.ListResult();
                var data = db.Games.AsQueryable();
                var size = 10;
                page = page - 1;

                data = data.OrderByDescending(f => f.Id);

                var c = data.Count();
                result.PagesCount = c / size;
                if (c % size > 0) result.PagesCount++;
                result.CurrentPage = page;

                var games = new List<Models.GameModel>();
                foreach(var item in data.Skip(page * size).Take(size).ToList())
                {
                    var g = new Models.GameModel();
                    g.Id = item.Id;
                    g.TotalQuestions = db.GameQuestions.Count(f => f.GameId == item.Id);
                    g.CorrectAnswers = db.GameQuestions.Count(f => f.GameId == item.Id && f.UserAnswerCorrect);
                    g.WrongAnswers = g.TotalQuestions - g.CorrectAnswers;
                    g.UserId = item.UserId;
                    g.UserFullName = db.Users.Where(f => f.Id == item.UserId).Select(f => f.FirstName + " " + f.LastName).FirstOrDefault();

                    g.Questions = new List<Models.QuestionItem>();

                    foreach (var q in db.GameQuestions.Where(f => f.GameId == item.Id).ToList())
                        g.Questions.Add(new Models.QuestionItem
                        {
                            Id = q.Id,
                            Text = db.Quotes.Where(f => f.Id == q.QuoteId).Select(f => f.Text).FirstOrDefault(),
                            AuthorName = db.Authors.Where(f => f.Id == db.Quotes.Where(a => a.Id == q.QuoteId).Select(a => a.AuthorId).FirstOrDefault()).Select(f => f.Name).FirstOrDefault(),
                            IsCorrect = q.UserAnswerCorrect,
                            UserAnswer = db.Authors.Where(f => f.Id == q.UserAnswer).Select(f => f.Name).FirstOrDefault()
                        });

                    games.Add(g);
                }
                result.Data = games;
                return CreateJsonResult(result);
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }




    }
}