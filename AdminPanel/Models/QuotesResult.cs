﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminPanel.Models
{
    public class QuotesResult
    {
        public List<Quote> Quotes { get; set; }
        public List<CoreLib.DataModels.Authors> Authors { get; set; }
        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }

    }


}
