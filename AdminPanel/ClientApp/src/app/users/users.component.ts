import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';




@Component({
  selector: 'app-users-component',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'] 
})

export class UsersComponent {
  public users: User[];
  public pages: number[];
  public currentPage: number;
  public createMode: boolean;
  public editId: number;
  public saving: boolean;
  public deleting: boolean;
  public errShow: boolean;
  public errText: string;
  public newAuthor: boolean;
  public deleteModalShow: boolean;
  public deleteSelectedItem: User;
  public model: User;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.pages = [];
    this.currentPage = 1;
    this.createMode = false;
    this.editId = 0;
    this.saving = false;
    this.deleting = false;
    this.errShow = false;
    this.newAuthor = false;
    this.deleteModalShow = false;

    this.clearModel();
    this.fetchData();
    


  }


  showErr(msg: string) {
    this.errText = msg;
    this.errShow = true;
  }

  closeModals() {
    this.deleteModalShow = false;
  }

  clearModel() {
    this.model = <User>{
      id: 0,
      firstName: '',
      lastName: '',
      password: '',
      repass: '',
      username: ''
    };
  }

  createNew() {
    this.clearModel();
    this.createMode = true;
    this.editId = 0;
  }
  createCancel() {
    this.createMode = false;
    this.saving = false;
    this.editId = 0;
  }

  editItem(item: User) {
    this.createMode = true;
    this.editId = item.id;
    this.model = item;
    this.model = <User>{
      ...this.model,
      password: '',
      repass: ''
    }
  }

  saveUser() {
    this.errShow = false;
    if (this.model.username == '') {
      this.showErr('Please fill the username!');
      return;
    }
    if (this.model.password == '' && this.editId == 0) {
      this.showErr('Please fill the password!');
      return;
    }
    if (this.model.firstName == '') {
      this.showErr('Please fill the first name!');
      return;
    }
    if (this.model.lastName == '') {
      this.showErr('Please fill the last name!');
      return;
    }

    if (this.model.repass != this.model.password && this.editId == 0) {
      this.showErr('Confirm password does not match to password!');
      return;
    }

    this.saving = true;
    let params = new HttpParams()
      .append("id", this.model.id.toString())
      .append("username", this.model.username)
      .append("password", this.model.password)
      .append("firstName", this.model.firstName)
      .append("lastName", this.model.lastName);
    this.http.post<SaveResult>(this.baseUrl + 'api/SaveUser', params).subscribe(result => {
      
      if (result.success) {
        if (this.editId > 0) {
          this.users.forEach(function (row) {
            if (row.id == result.data.id) {
              row.username = result.data.username;
              row.firstName = result.data.firstName;
              row.lastName = result.data.lastName;
            }
          });
        }
        else {
          this.users.unshift(result.data);
        }
        this.editId = 0;
        this.createMode = false;
        this.clearModel();
      }
      else {
        this.showErr(result.message);
      }

      this.saving = false;
     
    }, error => console.error(error));
  }

  deleteShow(item: User) {
    this.deleteSelectedItem = item;
    this.deleteModalShow = true;
  }
  deleteQuote() {
    this.deleting = true;
    let deleteId = this.deleteSelectedItem.id;
    let params = new HttpParams()
      .append("id", deleteId.toString());
    
    this.http.post<DeleteResult>(this.baseUrl + 'api/DeleteUser', params).subscribe(result => {
      if (result.data) {
        this.users = this.users.filter(function (row) {
          return row.id != deleteId;
        });

        this.deleteModalShow = false;
      }
      this.deleting = false;
    }, error => console.error(error));
  }

  goToPage(p) {
    this.currentPage = p;
    this.fetchData();
  }

  fetchData() {
    let params = new HttpParams()
      .append("page", this.currentPage.toString());
    this.http.post<FetchResult>(this.baseUrl + 'api/GetUsers', params).subscribe(result => {
      this.pages = [];
      this.users = result.data.data;
      for (var i = 0; i < result.data.pagesCount; i++) this.pages.push(i + 1);
    }, error => console.error(error));
  }

}

interface FetchResult {
  data: ListData,
  success: boolean,
  message: string
}
interface SaveResult {
  data: User,
  success: boolean,
  message: string
}
interface DeleteResult {
  data: boolean,
  success: boolean,
  message: string
}
interface ListData {
  data: User[],
  currentPage: number,
  pagesCount: number
}

interface User {
  id: number,
  username: string,
  password: string,
  repass: string,
  firstName: string,
  lastName: string
}
