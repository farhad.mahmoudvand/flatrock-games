import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { trigger, state, style, animate, transition } from '@angular/animations';




@Component({
  selector: 'app-quotes-component',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css'] 
})

export class QuotesComponent {
  public quotes: Quote[];
  public authors: Author[];
  public pages: number[];
  public currentPage: number;
  public createMode: boolean;
  public editId: number;
  public saving: boolean;
  public deleting: boolean;
  public errShow: boolean;
  public errText: string;
  public newAuthor: boolean;
  public deleteModalShow: boolean;
  public deleteSelectedItem: Quote;
  public model: {
    id: number,
    text: string,
    authorId: number,
    authorName: string
  };

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private elementRef: ElementRef) {
    this.pages = [];
    this.currentPage = 1;
    this.createMode = false;
    this.editId = 0;
    this.saving = false;
    this.deleting = false;
    this.errShow = false;
    this.newAuthor = false;
    this.deleteModalShow = false;
    this.model = {
      id: 0,
      text: '',
      authorId: 0,
      authorName: ''
    };
    this.fetchData();
    


  }


  showErrModal(msg: string) {
    this.errText = msg;
    this.errShow = true;
  }
  closeModals() {
    this.errShow = false;
    this.deleteModalShow = false;
  }

  toggleAuthors(a: boolean) {
    this.newAuthor = a;
    if (!a) {
      this.model = {
        ...this.model,
        authorName: ''
      };
    }
  }

  createNew() {
    this.model = {
      id: 0,
      text: '',
      authorId: 0,
      authorName: ''
    };
    this.createMode = true;
  }
  createCancel() {
    this.createMode = false;
    this.saving = false;
    this.editId = 0;
  }

  editItem(item: Quote) {
    this.editId = item.id;
    this.model = {
      id: item.id,
      authorId: item.authorId,
      authorName: '',
      text: item.text
    };
  }

  saveQuote() {
    if (this.model.text == '') {
      this.showErrModal('Please enter the quote text!');
      return;
    }
    if (this.model.authorId == 0 && !this.newAuthor) {
      this.showErrModal('Please select an author!');
      return;
    }
    if (this.model.authorName == '' && this.newAuthor) {
      this.showErrModal('Please enter the author name!');
      return;
    }
    this.saving = true;
    let params = new HttpParams()
      .append("id", this.model.id.toString())
      .append("text", this.model.text)
      .append("authorId", this.model.authorId.toString())
      .append("authorName", this.model.authorName);
    this.http.post<SaveResult>(this.baseUrl + 'api/SaveQuote', params).subscribe(result => {
      console.log(result);
      if (result.success) {
        if (this.editId > 0) {
          this.quotes.forEach(function (row) {
            if (row.id == result.data.id) {
              row.authorName = result.data.authorName;
              row.text = result.data.text;
              row.lastModified = result.data.lastModified;
            }
          });
        }
        else {
          this.quotes.unshift(result.data);
        }
        this.editId = 0;
        this.model = {
          ...this.model,
          id: 0,
          text: '',
          authorName: ''
        };
      }

      this.saving = false;
     
    }, error => console.error(error));
  }

  deleteShow(item: Quote) {
    this.deleteSelectedItem = item;
    this.deleteModalShow = true;
  }
  deleteQuote() {
    this.deleting = true;
    let deleteId = this.deleteSelectedItem.id;
    let params = new HttpParams()
      .append("id", deleteId.toString());
    
    this.http.post<DeleteResult>(this.baseUrl + 'api/DeleteQuote', params).subscribe(result => {
      if (result.data) {
        this.quotes = this.quotes.filter(function (row) {
          return row.id != deleteId;
        });

        this.deleteModalShow = false;
      }
      this.deleting = false;
    }, error => console.error(error));
  }

  goToPage(p) {
    this.currentPage = p;
    this.fetchData();
  }

  fetchData() {
    let params = new HttpParams()
      .append("page", this.currentPage.toString());
    this.http.post<FetchResult>(this.baseUrl + 'api/GetQuotes', params).subscribe(result => {
      this.pages = [];
      this.quotes = result.data.quotes;
      this.authors = result.data.authors;
      for (var i = 0; i < result.data.pagesCount; i++) this.pages.push(i + 1);
    }, error => console.error(error));
  }

}

interface FetchResult {
  data: ListData,
  success: boolean,
  message: string
}
interface SaveResult {
  data: Quote,
  success: boolean,
  message: string
}
interface DeleteResult {
  data: boolean,
  success: boolean,
  message: string
}
interface ListData {
  quotes: Quote[],
  authors: Author[],
  currentPage: number,
  pagesCount: number
}

interface Quote {
  id: number,
  authorId: number,
  authorName: string,
  text: string,
  lastModified: string
}

interface Author {
  id: number,
  name: string
}
