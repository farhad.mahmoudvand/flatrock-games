import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  public games: Game[];
  public pages: number[];
  public currentPage: number;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.pages = [];
    this.currentPage = 1;

    this.fetchData();
  }


  goToPage(p) {
    this.currentPage = p;
    this.fetchData();
  }

  fetchData() {
    let params = new HttpParams()
      .append("page", this.currentPage.toString());
    this.http.post<FetchResult>(this.baseUrl + 'api/GetLatestGames', params).subscribe(result => {
      this.pages = [];
      this.games = result.data.data;
      for (var i = 0; i < result.data.pagesCount; i++) this.pages.push(i + 1);
    }, error => console.error(error));
  }


}

interface FetchResult {
  data: ListData,
  success: boolean,
  message: string
}

interface ListData {
  data: Game[],
  currentPage: number,
  pagesCount: number
}

interface Game {
  id: number,
  totalQuestions: number,
  correctAnswers: number,
  wrongAnswers: number,
  userId: number,
  userFullName: string,
  questions: Question[]
}

interface Question {
  id: number,
  text: string,
  authorName: string,
  userAnswer: string,
  isCorrect: boolean
}
