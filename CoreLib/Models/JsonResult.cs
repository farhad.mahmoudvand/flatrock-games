﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreLib.Models
{
    public class JsonResult
    {
        public object data { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
    }

}
