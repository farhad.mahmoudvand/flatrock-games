﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreLib.Models
{
    public class ListResult
    {
        public object Data { get; set; }
        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }
    }
}
