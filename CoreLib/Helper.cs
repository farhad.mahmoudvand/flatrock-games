﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CoreLib
{
    public static class Helper
    {

        public static bool HasValue(this string data)
        {
            return data != null && data != "";
        }

        public static bool IsNull(this string data)
        {
            return data == null || data == "";
        }

        public static string HashMD5(this string data)
        {
            MD5CryptoServiceProvider provider1 = new MD5CryptoServiceProvider();
            byte[] buffer1 = provider1.ComputeHash(Encoding.UTF8.GetBytes(data));
            string text1 = "";
            for (int num1 = 0; num1 < buffer1.Length; num1++)
            {
                text1 = text1 + string.Format("{0:x02}", buffer1[num1]);
            }
            return text1;
        }


    }
}
