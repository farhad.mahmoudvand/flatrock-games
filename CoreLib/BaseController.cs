﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using CoreLib.DataModels;


namespace CoreLib
{
    public class BaseController : Controller
    {
        public quizgameContext db;

        public BaseController(quizgameContext context)
        {
            db = context;
        }
        
        public JsonResult CreateJsonResult(object data)
        {
            return Json(new Models.JsonResult
            {
                data = data,
                success = true,
                message = ""
            });
        }

        public JsonResult CreateJsonResult(object data, string message)
        {
            return Json(new Models.JsonResult
            {
                data = data,
                success = true,
                message = message
            });
        }

        public JsonResult CreateErrorResult(string error)
        {
            return Json(new Models.JsonResult
            {
                data = null,
                success = false,
                message = error
            });
        }





    }
}
