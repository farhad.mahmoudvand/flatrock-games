﻿using System;
using System.Collections.Generic;

namespace CoreLib.DataModels
{
    public partial class GameQuestionChoices
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public int AuthorId { get; set; }

        public virtual GameQuestions Question { get; set; }
    }
}
