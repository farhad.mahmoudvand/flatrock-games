﻿using System;
using System.Collections.Generic;

namespace CoreLib.DataModels
{
    public partial class Authors
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
