﻿using System;
using System.Collections.Generic;

namespace CoreLib.DataModels
{
    public partial class GameQuestions
    {
        public GameQuestions()
        {
            GameQuestionChoices = new HashSet<GameQuestionChoices>();
        }

        public int Id { get; set; }
        public int GameId { get; set; }
        public int QuoteId { get; set; }
        public int UserAnswer { get; set; }
        public bool UserAnswerCorrect { get; set; }

        public virtual Games Game { get; set; }
        public virtual ICollection<GameQuestionChoices> GameQuestionChoices { get; set; }
    }
}
