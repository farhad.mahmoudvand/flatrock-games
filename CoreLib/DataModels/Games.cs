﻿using System;
using System.Collections.Generic;

namespace CoreLib.DataModels
{
    public partial class Games
    {
        public Games()
        {
            GameQuestions = new HashSet<GameQuestions>();
        }

        public int Id { get; set; }
        public int Mode { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual ICollection<GameQuestions> GameQuestions { get; set; }
    }
}
