﻿using System;
using System.Collections.Generic;

namespace CoreLib.DataModels
{
    public partial class Quotes
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModified { get; set; }
    }
}
