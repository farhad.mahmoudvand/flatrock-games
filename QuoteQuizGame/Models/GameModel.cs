﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteQuizGame.Models
{
    public class GameModel
    {
        public int Id { get; set; }
        public int Current { get; set; }
        public bool CurrentAnswerd { get; set; }
        public List<QuoteItem> Quotes { get; set; }
    }

}
