﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteQuizGame.Models
{
    public class AuthorItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
