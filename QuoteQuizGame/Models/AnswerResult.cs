﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteQuizGame.Models
{
    public class AnswerResult
    {
        public int AnswerId { get; set; }
        public bool AnswerIsCorrect { get; set; }
        public int CorrectAuthorId { get; set; }
        public string CorrectAuthorName { get; set; }
        
    }
}
