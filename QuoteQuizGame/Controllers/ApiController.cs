﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreLib;
using CoreLib.DataModels;


namespace QuoteQuizGame.Controllers
{
    public class ApiController : CoreLib.BaseController
    {
        public ApiController(quizgameContext context) : base(context)
        {

        }

        public JsonResult LoginUser(string username, string password)
        {
            try
            {
                using (db = new quizgameContext())
                {
                    password = password.HashMD5();
                    var user = db.Users.FirstOrDefault(f => f.Username == username && f.Password == password);
                    if (user == null) return CreateErrorResult("Username or password is wrong!");
                    return CreateJsonResult(new Models.User
                    {
                        Id = user.Id,
                        Username = user.Username,
                        FirstName = user.FirstName,
                        LastName = user.LastName
                    });
                }
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }


        public JsonResult StartNewGame(int userId, int mode)
        {
            try
            {
                using (db = new quizgameContext())
                {
                    var result = new Models.GameModel();

                    var game = new Games();
                    game.Mode = mode;
                    game.UserId = userId;
                    game.CreateDate = DateTime.Now;
                    db.Games.Add(game);
                    db.SaveChanges();

                    result.Id = game.Id;
                    result.Current = 0;
                    result.CurrentAnswerd = false;
                    result.Quotes = new List<Models.QuoteItem>();
                    
                    var quotes = db.Quotes.OrderBy(f => Guid.NewGuid()).Take(5).ToList();
                    foreach (var q in quotes)
                    {
                        var gameQ = new GameQuestions();
                        gameQ.GameId = game.Id;
                        gameQ.QuoteId = q.Id;
                        gameQ.UserAnswer = 0;
                        gameQ.UserAnswerCorrect = false;
                        db.GameQuestions.Add(gameQ);
                        db.SaveChanges();

                        var qItem = new Models.QuoteItem();
                        qItem.Id = q.Id;
                        qItem.QuestionId = gameQ.Id;
                        qItem.Text = q.Text;
                        qItem.Authors = new List<Models.AuthorItem>();
                        
                        foreach (var au in db.Authors.OrderByDescending(f => f.Id == q.AuthorId)
                            .ThenBy(f => Guid.NewGuid()).Take(mode == 1 ? 2 : 3).ToList()
                            .OrderBy(f=> Guid.NewGuid()))
                        {
                            var ch = new GameQuestionChoices();
                            ch.QuestionId = gameQ.Id;
                            ch.AuthorId = au.Id;
                            db.GameQuestionChoices.Add(ch);
                            db.SaveChanges();

                            qItem.Authors.Add(new Models.AuthorItem
                            {
                                Id = au.Id,
                                Name = au.Name
                            });

                        }

                        result.Quotes.Add(qItem);
                    }
                    
                    return CreateJsonResult(result);
                }

            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult AnswerQuestion(int gameId, int qId, int answer)
        {
            try
            {
                using (db = new quizgameContext())
                {
                    var result = new Models.AnswerResult();
                    var q = db.GameQuestions.FirstOrDefault(f => f.GameId == gameId && f.Id == qId);
                    if (q == null) return CreateErrorResult("Question not found!");

                    result.CorrectAuthorId = db.Quotes.Where(f => f.Id == q.QuoteId).Select(f => f.AuthorId).FirstOrDefault();
                    result.CorrectAuthorName = db.Authors.Where(f => f.Id == result.CorrectAuthorId).Select(f => f.Name).FirstOrDefault();
                    result.AnswerIsCorrect = answer == result.CorrectAuthorId;
                    result.AnswerId = answer;
                    q.UserAnswer = answer;
                    q.UserAnswerCorrect = result.AnswerIsCorrect;
                    db.SaveChanges();

                    return CreateJsonResult(result);
                }
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

        public JsonResult FinishGame(int gameId)
        {
            try
            {
                using (db = new quizgameContext())
                {
                    var result = new Models.GameResult();
                    var game = db.Games.Find(gameId);
                    if (game == null) return CreateErrorResult("Game not found!");
                    result.TotalQuestions = db.GameQuestions.Count(f=>f.GameId == gameId);
                    result.CorrectAnswers = db.GameQuestions.Count(f => f.GameId == gameId && f.UserAnswerCorrect);
                    result.WrongAnswers = result.TotalQuestions - result.CorrectAnswers;
                    result.GameId = gameId;
                    return CreateJsonResult(result);
                }
            }
            catch (Exception ex)
            {
                return CreateErrorResult(ex.Message);
            }
        }

    }
}