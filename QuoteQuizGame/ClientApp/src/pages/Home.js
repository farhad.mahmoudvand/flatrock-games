import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import $ from "jquery";
import QuoteBox from "../components/QuoteBox";
import ChoiceBox from "../components/ChoiceBox";
import SubmitButton from "../components/SubmitButton";
import * as actions from "../redux/actions";

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starting: false
        }
        
        this.startNewGame = this.startNewGame.bind(this);
        
    }
    
    componentDidMount() {
        if (this.props.user.id === 0) this.props.history.push('/login');
    }
    

    async startNewGame() {
        this.setState({
            starting: true
        });

        var data = new FormData();
        data.append("userId", this.props.user.id);
        data.append("mode", this.props.gameMode);
        await axios
            .post("/api/StartNewGame", data)
            .then(res => {
                this.props.startNewGame(res.data.data);
                this.setState({
                    starting: false
                });
            })
            .catch(err => {
                console.log(err);
            });

        
    }

    
    render() {
        return (
            <div>
                <div className="container">
                    {
                        this.props.gameResult &&
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 p-5">
                                <div className="card">
                                    <div className="card-header bg-success text-white">
                                        <h4>Latest Game Result</h4>
                                    </div>
                                    <div className="card-body card-relative">
                                        <div>
                                            <b>Total Questions: </b>
                                            {this.props.gameResult.totalQuestions}
                                        </div>
                                        <div>
                                            <b>Correct Answers: </b>
                                            {this.props.gameResult.correctAnswers}
                                        </div>
                                        <div>
                                            <b>Wrong Answers: </b>
                                            {this.props.gameResult.wrongAnswers}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    }
                    {
                        this.props.currentGame ?
                            <div className="row mt-1">
                                <div className="col col-sm-8">
                                    <QuoteBox />
                                </div>
                                <div className="col col-sm-4">
                                    <ChoiceBox />
                                    <div>&nbsp;</div>
                                    {
                                        this.props.currentGame.currentAnswerd &&
                                        <SubmitButton />
                                    }
                                </div>
                            </div>
                            :
                            <div className="row">
                                <div className="col text-center p-5">
                                    {
                                        this.state.starting ?
                                            <button type="button" className="btn btn-info" onClick={this.startNewGame} style={{ width: '150px' }} disabled>
                                                <i className="fa fa-spin fa-spinner"></i>
                                            </button>
                                            :
                                            <button type="button" className="btn btn-info" onClick={this.startNewGame} style={{ width: '150px' }}>
                                                Start New Game
                                            </button>
                                    }
                                </div>
                            </div>
                            

                    }
                    
                    
                </div>
            </div>
        );
    }
}

const mapStateProps = state => {
    return {
        user: state.user,
        gameMode: state.gameMode,
        currentGame: state.currentGame,
        gameResult: state.gameResult
    };
};

const mapDispatchProps = dispatch => {
    return {
        startNewGame: game => {
            dispatch(actions.setCurrentGame(game));
        }
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(Home);