import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import * as actions from "../redux/actions";

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            alertBox: {
                show: false,
                message: ''
            }
        };

        this.login = this.login.bind(this);
        this.handleError = this.handleError.bind(this);
    }

    handleError(msg = "") {
        this.setState({
            alertBox: {
                show: msg != '',
                message: msg
            }
        });
    }

     async login(e) {
        this.handleError();
        e.preventDefault();
        const formData = new FormData(e.target);
        var username = formData.get("username");
        var password = formData.get("password");
        console.log(username, password);
        if (username === '') {
            this.handleError('Please fill the username!');
            return;
        }
        if (password === '') {
            this.handleError('Please fill the password!');
            return;
        }

         await axios
             .post("/api/LoginUser", formData)
            .then(res => {
                console.log(res);
                if (res.data.success) {
                    this.props.login(res.data.data);
                    this.props.history.push("/");
                }
                else {
                    this.handleError(res.data.message);
                }
            })
            .catch(err => {
                console.log(err);
            });
        
    }


    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            &nbsp;
                        </div>
                    </div>
                    <div className="clearfix">&nbsp;</div>
                    <div className="row">
                        <div className="col col-md-6 offset-md-3">
                            <div className="card">
                                <div className="card-header bg-info text-white">
                                    <h4>Login</h4>
                                </div>
                                <div className="card-body">
                                    {
                                        this.state.alertBox.show &&
                                        <div className="alert alert-danger">{this.state.alertBox.message}</div>
                                    }
                                    <form onSubmit={this.login}>
                                        <div className="form-group">
                                            <label htmlFor="txtusername">Username</label>
                                            <input type="text" className="form-control" name="username" id="txtusername" autoFocus />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtpassword">Password</label>
                                            <input type="password" className="form-control" name="password" id="txtpassword" />
                                        </div>
                                        <div className="form-group">
                                            <button type="submit" className="btn btn-info" style={{ width: '150px' }}>
                                                Login
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateProps = state => {
    return {

    };
};

const mapDispatchProps = dispatch => {
    return {
        login: async user => {
            dispatch(actions.login(user));
        },
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(Login);