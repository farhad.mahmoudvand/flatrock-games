import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../redux/actions";

class Logout extends Component {

    constructor(props) {
        super(props);

    }

    componentWillMount() {
        this.props.logout();
        this.props.history.push("/login");
    }

    render() {
        return (
            <div></div>
        );
    }
}

const mapStateProps = state => {
    return {
        
    };
};

const mapDispatchProps = dispatch => {
    return {
        logout: () => {
            dispatch(actions.logout());
        },
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(Logout);