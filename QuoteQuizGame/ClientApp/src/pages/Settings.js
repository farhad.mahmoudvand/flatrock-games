import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../redux/actions";

class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            gameMode: this.props.gameMode
        };

        this.modeChange = this.modeChange.bind(this);
    }

    componentDidMount() {
        if (this.props.user.id === 0) this.props.history.push('/login');
    }


    modeChange(m) {
        this.setState({
            gameMode: m
        });

        this.props.setMode(m);
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col col-sm-8 offset-sm-2 p-5">
                            <div className="card">
                                <div className="card-header bg-info text-white">
                                    <h4>Game Mode</h4>
                                </div>
                                <div className="card-body">
                                    {
                                        this.props.currentGame == null ?
                                            <form>
                                                <div className="custom-control custom-radio m-2">
                                                    <input type="radio" className="custom-control-input" id="modeYesOrNor" name="gameMode" value="1" defaultChecked={this.state.gameMode === "1"} onClick={() => this.modeChange("1")} />
                                                    <label className="custom-control-label" htmlFor="modeYesOrNor">Yes/No</label>
                                                </div>
                                                <div className="custom-control custom-radio m-2">
                                                    <input type="radio" className="custom-control-input" id="modeMultiple" name="gameMode" value="2" defaultChecked={this.state.gameMode === "2"} onClick={() => this.modeChange("2")} />
                                                    <label className="custom-control-label" htmlFor="modeMultiple">Multiple Choices</label>
                                                </div>
                                            </form>
                                            :
                                            <div>You can't change the settings during the game!</div>
                                    }
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateProps = state => {
    return {
        user: state.user,
        gameMode: state.gameMode,
        currentGame: state.currentGame
    };
};

const mapDispatchProps = dispatch => {
    return {
        setMode: mode => {
            dispatch(actions.setMode(mode));
        }
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(Settings);