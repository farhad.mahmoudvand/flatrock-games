﻿import * as storage from "../storage";

export const InitialState = {
    appLoading: true,
    appLoaded: false,
    user: storage.userGet(),
    gameMode: storage.modeGet(),
    currentAnswerResult: null,
    currentGame: null,
    gameResult: null
};