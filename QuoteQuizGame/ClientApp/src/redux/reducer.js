﻿import { InitialState } from "./initialState";
import * as storage from "../storage";
export default function rootReducer(state = InitialState, action) {

    if (action.type === "LOGIN_USER") {
        storage.userSave(action.user);
        return {
            ...state,
            user: action.user 
        };
    }
    else if (action.type === "LOGOUT_USER") {
        storage.userClear();
        return {
            ...state,
            user: storage.userGet(),
            currentAnswerResult: null,
            currentGame: null,
            gameResult: null
        };
    }
    else if (action.type === "SET_GAME_MODE") {
        storage.modeSet(action.mode);
        return {
            ...state,
            gameMode: action.mode
        };
    }

    else if (action.type === "SET_CURRENT_GAME") {
        return {
            ...state,
            currentGame: action.game,
            gameResult: null
        };
    }
    else if (action.type === "SET_CURRENT_QUESTION") {
        return {
            ...state,
            currentGame: {
                ...state.currentGame,
                current: action.q,
                currentAnswerd: false
            }
        };
    }
    else if (action.type === "SET_CURRENT_ANSWER") {
        return {
            ...state,
            currentAnswerResult: action.answer,
            currentGame: {
                ...state.currentGame,
                currentAnswerd: true
            }
        };
    }
    else if (action.type === "FINISH_CURRENT_GMAE") {
        return {
            ...state,
            currentGame: null,
            currentAnswerResult: null,
            gameResult: action.result
        }
    }
    else return state;

}
