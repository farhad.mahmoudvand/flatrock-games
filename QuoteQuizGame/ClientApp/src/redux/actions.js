﻿export function login(user) {
    return {
        type: "LOGIN_USER",
        user: user
    };
}

export function logout() {
    return {
        type: "LOGOUT_USER"
    };
}


export function setMode(mode) {
    return {
        type: "SET_GAME_MODE",
        mode: mode
    };
}

export function setCurrentGame(game) {
    return {
        type: "SET_CURRENT_GAME",
        game: game
    };
}

export function setCurrentQuestion(q) {
    return {
        type: "SET_CURRENT_QUESTION",
        q: q
    };
}

export function setCurrentAnswer(answer) {
    return {
        type: "SET_CURRENT_ANSWER",
        answer: answer
    };
}

export function finishCurrentGame(result) {
    return {
        type: "FINISH_CURRENT_GMAE",
        result: result
    };
}