﻿import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import $ from "jquery";
import * as actions from "../redux/actions";

class SubmitButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answering: false
        }
        this.nextQuote = this.nextQuote.bind(this);
        this.finishdGame = this.finishdGame.bind(this);
    }
    
    nextQuote() {
        $(".btn-choices").removeClass("btn-success").removeClass("btn-danger").addClass("btn-light");
        this.props.showQuestion(this.props.currentGame.current + 1);
    }

    async finishdGame() {

        var data = new FormData();
        data.append("gameId", this.props.currentGame.id);
        await axios
            .post("/api/FinishGame", data)
            .then(res => {
                this.props.finishGame(res.data.data);
            })
            .catch(err => {
                console.log(err);
            });


    }

    render() {
        return (
            <div>
                {
                    this.props.currentGame.current + 1 >= this.props.currentGame.quotes.length ?
                        <button className="btn btn-block btn-dark" onClick={this.finishdGame}>Finish The Game</button>
                        :
                        <button className="btn btn-block btn-dark" onClick={this.nextQuote}>Next Quote</button>
                }

            </div>
        );
    }
}

const mapStateProps = state => {
    return {
        gameMode: state.gameMode,
        currentGame: state.currentGame
    };
};

const mapDispatchProps = dispatch => {
    return {
        showQuestion: q => {
            dispatch(actions.setCurrentQuestion(q));
        },
        finishGame: gameResult => {
            dispatch(actions.finishCurrentGame(gameResult));
        }
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(SubmitButton);