import React from 'react';
import { connect } from "react-redux";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';
import * as actions from "../redux/actions";

class NavMenu extends React.Component {
  constructor (props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle () {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render () {
    return (
        <header>
            <Navbar className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light >
                <Container>
                    <NavbarBrand tag={Link} to="/">Quote Quiz Game</NavbarBrand>
                    {
                        this.props.user.id > 0 &&
                        <NavbarToggler onClick={this.toggle} className="mr-2" />
                    }
                    {
                        this.props.user.id > 0 &&
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={this.state.isOpen} navbar>
                            <div>
                                |
                                Welcome {this.props.user.firstName}
                                </div>
                            <ul className="navbar-nav flex-grow">
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/">Play</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/settings">Settings</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/logout">Logout</NavLink>
                                </NavItem>
                            </ul>
                        </Collapse>
                    }
                </Container>
            </Navbar>
        </header>
    );
  }
}

const mapStateProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchProps = dispatch => {
    return {
        
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(NavMenu);