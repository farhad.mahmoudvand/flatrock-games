﻿import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import * as actions from "../redux/actions";

class QuoteBox extends Component {

    constructor(props) {
        super(props);
        
    }


    render() {
        return (
            <div className="card">
                <div className="card-header bg-info text-white">
                    <h4>Quote {this.props.currentGame.current + 1}/{this.props.currentGame.quotes.length}</h4>
                </div>
                <div className="card-body bg-dark">
                    <div className="blockquote-wrapper">
                        <div className="blockquote">
                            <h1>
                                {this.props.currentGame.quotes[this.props.currentGame.current].text}
                            </h1>
                            {
                                this.props.gameMode === "1" && !this.props.currentGame.currentAnswerd &&
                                <h4>
                                    {this.props.currentGame.quotes[this.props.currentGame.current].authors[0].name}
                                </h4>
                            }
                            {
                                this.props.currentGame.currentAnswerd &&
                                <h4 className="text-success">
                                    {this.props.currentAnswerResult.correctAuthorName}
                                </h4>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateProps = state => {
    return {
        gameMode: state.gameMode,
        currentAnswerResult: state.currentAnswerResult,
        currentGame: state.currentGame
    };
};

const mapDispatchProps = dispatch => {
    return {
        
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(QuoteBox);