﻿import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import $ from "jquery";
import * as actions from "../redux/actions";

class ChoiceBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answering: false
        }
        this.submitAnswer = this.submitAnswer.bind(this);
    }
    

    async submitAnswer(id) {

        if (this.props.currentGame.currentAnswerd) return;

        this.setState({
            answering: true
        });

        var data = new FormData();
        data.append("gameId", this.props.currentGame.id);
        data.append("qId", this.props.currentGame.quotes[this.props.currentGame.current].questionId);
        data.append("answer", id);
        await axios
            .post("/api/AnswerQuestion", data)
            .then(res => {
                if (this.props.gameMode === "1") {
                    $("#choice_" + res.data.data.answerId).removeClass("btn-light").addClass(res.data.data.answerIsCorrect ? "btn-success" : "btn-danger");
                    
                }
                else {
                    $("#choice_" + res.data.data.correctAuthorId).removeClass("btn-light").addClass("btn-success");
                    if (!res.data.data.answerIsCorrect) $("#choice_" + res.data.data.answerId).removeClass("btn-light").addClass("btn-danger");
                }
                
                this.setState({
                    answering: false
                });
                this.props.setCurrentAnswer(res.data.data);
            })
            .catch(err => {
                console.log(err);
            });


    }

    render() {
        return (
            <div className="card">
                <div className="card-header bg-info text-white">
                    <h4>Pick The Answer</h4>
                </div>
                <div className="card-body card-relative">
                    {
                        this.props.gameMode === "1" ?
                            <div>
                                <div className="mb-2">
                                    <button id={'choice_' + this.props.currentGame.quotes[this.props.currentGame.current].authors[0].id} className="btn btn-block btn-choices btn-light" onClick={() => this.submitAnswer(this.props.currentGame.quotes[this.props.currentGame.current].authors[0].id)}>Yes</button>
                                </div>
                                <div className="mb-2">
                                    <button id={'choice_' + this.props.currentGame.quotes[this.props.currentGame.current].authors[1].id} className="btn btn-block btn-choices btn-light" onClick={() => this.submitAnswer(this.props.currentGame.quotes[this.props.currentGame.current].authors[1].id)}>No</button>
                                </div>
                            </div>

                            :
                            <div>
                                {
                                    this.props.currentGame.quotes[this.props.currentGame.current].authors.map((item, key) =>
                                        <div key={key} className="mb-2">
                                            <button id={'choice_' + item.id} className="btn btn-block btn-choices btn-light" onClick={() => this.submitAnswer(item.id)}>{item.name}</button>
                                        </div>
                                    )
                                }
                            </div>
                    }

                    {
                        this.state.answering &&
                        <div className="card-answering">
                            <i className="fa fa-spin fa-spinner"></i>
                        </div>
                    }

                </div>
            </div>
        );
    }
}

const mapStateProps = state => {
    return {
        gameMode: state.gameMode,
        currentGame: state.currentGame
    };
};

const mapDispatchProps = dispatch => {
    return {
        setCurrentAnswer: answer => {
            dispatch(actions.setCurrentAnswer(answer));
        }
    };
};


export default connect(
    mapStateProps,
    mapDispatchProps
)(ChoiceBox);