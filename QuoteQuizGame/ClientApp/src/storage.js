﻿
export function userSave(user) {
    localStorage.setItem("user", JSON.stringify(user));
}

export function userGet() {
    if (localStorage.getItem("user") == null)
        return {
            id: 0,
            username: '',
            firstName: '',
            lastName: ''
        };
    else return JSON.parse(localStorage.getItem("user"));
}

export function userClear() {
    localStorage.removeItem("user");
}

export function modeGet() {
    if (localStorage.getItem("mode") == null) return "1";
    else return localStorage.getItem("mode");
}
export function modeSet(mode) {
    localStorage.setItem("mode", mode);
}